/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
module.exports = {
  root: true,
  env: {
    browser: true,
    es2017: true,
    node: true,
  },
  parser: "@typescript-eslint/parser",
  extends: ["eslint:recommended", "airbnb-base"],
  plugins: ["@typescript-eslint"],
  overrides: [
    {
      files: ["*.ts"],
      extends: [
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
      ],
      parserOptions: {
        tsconfigRootDir: __dirname,
        project: ["./tsconfig.json"],
        parser: "@typescript-eslint/parser",
      },
      rules: {
        "comma-dangle": "off",
        "comma-spacing": "off",
        "brace-style": "off",
        "no-extra-semi": "off",
        "no-self-assign": "off",
        "function-paren-newline": "off",
        "require-await": "off",
        "default-param-last": 0,
        "object-curly-spacing": "off",
        "@typescript-eslint/comma-dangle": ["error", "always-multiline"],
        "@typescript-eslint/comma-spacing": ["error", { before: false, after: true },
        ],
        "@typescript-eslint/brace-style": ["error", "1tbs"],
        "@typescript-eslint/no-extra-semi": ["error"],
        "@typescript-eslint/require-await": ["error"],
        "@typescript-eslint/object-curly-spacing": ["error", "always"],
        "@typescript-eslint/type-annotation-spacing": [
          "error",
          {
            before: false,
            after: true,
          },
        ],
        indent: ["error", 2, {
          FunctionDeclaration: { parameters: "first" },
          FunctionExpression: { parameters: "first" },
        }],
      },
    },
  ],
  settings: {
    "import/resolver": {
      node: {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
      },
    },
  },
  parserOptions: {
    sourceType: "module",
    ecmaVersion: 2019,
  },
  rules: {
    indent: ["error", 2],
    "no-useless-constructor": 0,
    "no-restricted-syntax": 0,
    "class-methods-use-this": 0,
    quotes: ["error", "double"],
    semi: ["error", "always"],
    "linebreak-style": ["error", "unix"],
    "no-param-reassign": [
      "error",
      {
        props: false,
      },
    ],
    "no-unused-vars": [
      "error",
      {
        args: "after-used",
        argsIgnorePattern: "_",
      },
    ],
    "@typescript-eslint/no-unused-vars": [
      "error",
      {
        args: "after-used",
        argsIgnorePattern: "_",
      },
    ],
    "import/prefer-default-export": ["off"],
    "comma-dangle": ["error", "always-multiline"],
    "comma-spacing": [
      "error",
      {
        before: false,
        after: true,
      },
    ],
    "brace-style": ["error", "1tbs"],
    "no-extra-semi": ["error"],
    "require-await": ["error"],
    "require-yield": ["error"],
    "object-curly-spacing": ["error", "always"],
    "array-bracket-spacing": ["error", "never"],
    "import/extensions": "off",
    "no-plusplus": "off",
    "import/no-extraneous-dependencies": "off",
    "no-shadow": "off",
    "@typescript-eslint/no-shadow": ["error"],
  },
};
