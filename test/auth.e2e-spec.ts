import * as request from "supertest";
import { INestApplication } from "@nestjs/common";
import { DefaultAppModuleConfig } from "./default-test-config";
import { User } from "../src/users/entities/user.entity";
import { UserSeedService } from "./database/seeders/user/user-seed.service";

describe("AuthController", () => {
  let app: INestApplication;
  let userSeedManager: UserSeedService;

  beforeAll(async () => {
    app = await DefaultAppModuleConfig.initApp();
    userSeedManager = app.get(UserSeedService);
  });

  beforeEach(async () => {
    await userSeedManager.populate();
  });

  it("should get a token with valid user", async () => {
    const result = await request(app.getHttpServer())
      .post("/auth/login")
      .send(userSeedManager.all[0].seed);

    expect(result.status).toBe(200);
    expect(result.body.access_token).toBeDefined();
  });

  it("should not get a token if password is invalid", async () => {
    const user: User = userSeedManager.all[0].entity;
    const loginData = { email: user.email, password: "aaaaaaa" };

    const result = await request(app.getHttpServer())
      .post("/auth/login")
      .send(loginData);

    expect(result.status).toBe(401);
    expect(result.body.access_token).toBeUndefined();
  });

  it("should not get a token if email does not exists", async () => {
    const user: User = userSeedManager.all[0].entity;
    const loginData = { email: "email@invalid.com", password: user.password };

    const result = await request(app.getHttpServer())
      .post("/auth/login")
      .send(loginData);

    expect(result.status).toBe(401);
    expect(result.body.access_token).toBeUndefined();
  });

  it("should not get a token with empty email", async () => {
    const user: User = userSeedManager.all[0].entity;
    const loginData = { password: user.password };

    const result = await request(app.getHttpServer())
      .post("/auth/login")
      .send(loginData);

    expect(result.status).toBe(401);
    expect(result.body.access_token).toBeUndefined();
  });

  it("should not get a token with empty password", async () => {
    const user: User = userSeedManager.all[0].entity;
    const loginData = { email: user.email };

    const result = await request(app.getHttpServer())
      .post("/auth/login")
      .send(loginData);

    expect(result.status).toBe(401);
    expect(result.body.access_token).toBeUndefined();
  });

  afterEach(async () => {
    await userSeedManager.clean();
  });

  afterAll(async () => {
    await app.close();
  });
});
