import {
  INestApplication, ModuleMetadata, Type, ValidationPipe,
} from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { Test, TestingModule } from "@nestjs/testing";
import { AuthService } from "../src/auth/auth.service";
import { User } from "../src/users/entities/user.entity";
import { ModuleConfig } from "../src/app.module";
import { UserSeedModule } from "./database/seeders/user/user-seed.module";
import { RoleSeedModule } from "./database/seeders/role/role-seed.module";

export class DefaultAppModuleConfig {
  public static moduleFixture: TestingModule;

  public static async getModuleFixture(imports: Type[] = []): Promise<TestingModule> {
    this.setEnvVariables();

    const moduleConfig: ModuleMetadata = ModuleConfig();
    moduleConfig.imports.push(
      ...imports,
      UserSeedModule,
      RoleSeedModule,
      JwtModule.register({
        signOptions: { expiresIn: "60s" },
        secret: process.env.SECRET_KEY,
      }),
    );

    this.moduleFixture = await Test.createTestingModule(moduleConfig).compile();
    return this.moduleFixture;
  }

  public static async getNestApplication(imports?: Type[]): Promise<INestApplication> {
    await DefaultAppModuleConfig.getModuleFixture(imports);
    return this.moduleFixture.createNestApplication();
  }

  public static async initApp(imports?: Type[]): Promise<INestApplication> {
    const app = await DefaultAppModuleConfig.getNestApplication(imports);
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    return app;
  }

  public static getToken(user: User): string {
    const token: { [key: string]: string } = this.authService.login(user);
    return token.access_token;
  }

  public static get authService(): AuthService {
    return this.moduleFixture.get(AuthService);
  }

  private static setEnvVariables(): void {
    process.env.SECRET_KEY = "JWT TEST SECRET KEY";
    process.env.DB_NAME = ":memory:";
    process.env.TYPE = "sqlite";
    process.env.NODE_ENV = "test";
    process.env.DB_USER = "";
    process.env.DB_PASS = "";
    process.env.DB_HOST = "";
    process.env.DB_PORT = "";
  }
}
