import * as request from "supertest";
import { INestApplication } from "@nestjs/common";
import { DefaultAppModuleConfig } from "./default-test-config";
import { Role } from "../src/role/entities/role.entity";
import { UserSeedService } from "./database/seeders/user/user-seed.service";
import { RoleSeedService } from "./database/seeders/role/role-seed.service";
import { RoleSeeder } from "./database/seeders/role/role.seeder";

describe("RoleController", () => {
  let app: INestApplication;
  let userSeedManager: UserSeedService;
  let roleSeedManager: RoleSeedService;

  function getToken(isAdmin = false): string {
    if (isAdmin) {
      return DefaultAppModuleConfig.getToken(userSeedManager.admins[0].entity);
    }

    return DefaultAppModuleConfig.getToken(userSeedManager.nonAdmins[0].entity);
  }

  beforeAll(async () => {
    app = await DefaultAppModuleConfig.initApp();
    userSeedManager = app.get(UserSeedService);
    roleSeedManager = userSeedManager.roleSeedService;
  });

  beforeEach(async () => {
    await userSeedManager.populate();
  });

  it("should retrieve all roles", async () => {
    const expectedResult: Array<{ [key: string]: any }> = roleSeedManager.dataSeed;

    const token = getToken(true);
    const result = await request(app.getHttpServer())
      .get("/role")
      .set("Authorization", `Bearer ${token}`);

    expect(result.status).toBe(200);
    expect(result.body).toStrictEqual(expectedResult);
  });

  it("should not get roles if is not admin", async () => {
    const token = getToken();
    const result = await request(app.getHttpServer())
      .get("/role")
      .set("Authorization", `Bearer ${token}`);

    expect(result.status).toBe(403);
    expect(result.body.error).toBe("Forbidden");
  });

  it("should get a role", async () => {
    const role: RoleSeeder = roleSeedManager.all[0];

    const token = getToken(true);
    const result = await request(app.getHttpServer())
      .get(`/role/${role.entity.id}`)
      .set("Authorization", `Bearer ${token}`);

    expect(result.status).toBe(200);
    expect(result.body).toStrictEqual(role.seed);
  });

  it("should not get a role if is not admin", async () => {
    const token = getToken();
    const result = await request(app.getHttpServer())
      .get("/role/1")
      .set("Authorization", `Bearer ${token}`);

    expect(result.status).toBe(403);
    expect(result.body.error).toBe("Forbidden");
  });

  it("should update a role", async () => {
    const role: Role = roleSeedManager.all[0].entity;

    const token = getToken(true);
    const result = await request(app.getHttpServer())
      .patch(`/role/${role.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send({ name: "New name" });

    expect(result.status).toBe(200);
    expect(result.body.affected).toBe(1);
  });

  it("should not update a role if not admin", async () => {
    const token = getToken();
    const result = await request(app.getHttpServer())
      .patch("/role/1")
      .set("Authorization", `Bearer ${token}`)
      .send({ name: "New name" });

    expect(result.status).toBe(403);
    expect(result.body.error).toBe("Forbidden");
  });

  it("should create a role", async () => {
    const newRole: { [key: string]: any } = {
      name: "New role",
    };

    const token = getToken(true);
    const result = await request(app.getHttpServer())
      .post("/role")
      .set("Authorization", `Bearer ${token}`)
      .send(newRole);

    expect(result.status).toBe(201);
    expect(result.body).toHaveProperty("id");
    expect(result.body).toHaveProperty("name");
  });

  it("should not create a role if not admin", async () => {
    const newRole: { [key: string]: any } = {
      name: "New role",
    };

    const token = getToken();
    const result = await request(app.getHttpServer())
      .post("/role")
      .set("Authorization", `Bearer ${token}`)
      .send(newRole);

    expect(result.status).toBe(403);
    expect(result.body.error).toBe("Forbidden");
  });

  it("should delete a role", async () => {
    const role: Role = roleSeedManager.all[0].entity;

    const token = getToken(true);
    const result = await request(app.getHttpServer())
      .delete(`/role/${role.id}`)
      .set("Authorization", `Bearer ${token}`);

    expect(result.status).toBe(200);
    expect(result.body.affected).toBe(1);
  });

  it("should not delete a role if not admin", async () => {
    const role: Role = roleSeedManager.all[0].entity;

    const token = getToken();
    const result = await request(app.getHttpServer())
      .delete(`/role/${role.id}`)
      .set("Authorization", `Bearer ${token}`);

    expect(result.status).toBe(403);
    expect(result.body.error).toBe("Forbidden");
  });

  afterEach(async () => {
    await userSeedManager.clean();
  });

  afterAll(async () => {
    await app.close();
  });
});
