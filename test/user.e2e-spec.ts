import { INestApplication } from "@nestjs/common";
import * as request from "supertest";
import { DefaultAppModuleConfig } from "./default-test-config";
import { UserSeedService } from "./database/seeders/user/user-seed.service";
import { UserSeeder } from "./database/seeders/user/user.seeder";

describe("UserController", () => {
  let app: INestApplication;
  let userSeedManager: UserSeedService;

  beforeAll(async () => {
    app = await DefaultAppModuleConfig.initApp();
    userSeedManager = app.get(UserSeedService);
  });

  beforeEach(async () => {
    await userSeedManager.populate();
  });

  it("should retrieve all users", async () => {
    const expectedResult: Array<{ [key: string]: any }> = [];

    for (const user of userSeedManager.all) {
      expectedResult.push(user.responseHttp);
    }

    const token = DefaultAppModuleConfig.getToken(userSeedManager.admins[0].entity);
    const result = await request(app.getHttpServer())
      .get("/users")
      .set("Authorization", `Bearer ${token}`);

    expect(result.status).toBe(200);
    expect(result.body).toStrictEqual(expectedResult);
  });

  it("should get a user", async () => {
    const user: UserSeeder = userSeedManager.nonAdmins[0];
    const token = DefaultAppModuleConfig.getToken(user.entity);
    const result = await request(app.getHttpServer())
      .get(`/users/${user.entity.id}`)
      .set("Authorization", `Bearer ${token}`);

    expect(result.status).toBe(200);
    expect(result.body).toStrictEqual(user.responseHttp);
  });

  it("should not get a user, if not authenticated", async () => {
    const result = await request(app.getHttpServer()).get("/users/1");
    expect(result.status).toBe(401);
    expect(result.body.message).toBe("Unauthorized");
  });

  it("should update a user", async () => {
    const user: UserSeeder = userSeedManager.nonAdmins[0];
    const token = DefaultAppModuleConfig.getToken(user.entity);

    const result = await request(app.getHttpServer())
      .patch(`/users/${user.entity.id}`)
      .set("Authorization", `Bearer ${token}`)
      .send({ name: "New name" });

    expect(result.status).toBe(200);
    expect(result.body.affected).toBe(1);
  });

  it("should not update a user with an existing email", async () => {
    const user: UserSeeder = userSeedManager.nonAdmins[0];

    const token = DefaultAppModuleConfig.getToken(user.entity);
    const result = await request(app.getHttpServer())
      .patch("/users/1")
      .set("Authorization", `Bearer ${token}`)
      .send({ email: user.entity.email });

    expect(result.status).toBe(500);
  });

  it("should not update a user with an invalid email", async () => {
    const token = DefaultAppModuleConfig.getToken(userSeedManager.nonAdmins[0].entity);
    const result = await request(app.getHttpServer())
      .patch("/users/1")
      .set("Authorization", `Bearer ${token}`)
      .send({ email: "user.email" });

    expect(result.status).toBe(400);
    expect(result.body.message).toContainEqual("email must be an email");
  });

  it("should create a user", async () => {
    const newUser = {
      name: "New user",
      email: "newuser@email.com",
      password: "123456",
      roles: [],
    };

    const result = await request(app.getHttpServer())
      .post("/users")
      .send(newUser);

    expect(result.status).toBe(201);
    expect(result.body).toHaveProperty("id");
    expect(result.body).toHaveProperty("name");
    expect(result.body).toHaveProperty("email");
    expect(result.body).toHaveProperty("roles");
  });

  it("should not create a user with invalid email", async () => {
    const newUser = {
      name: "New user",
      email: "newuser.com",
      password: "123456",
      roles: [],
    };

    const result = await request(app.getHttpServer())
      .post("/users")
      .send(newUser);

    expect(result.status).toBe(400);
    expect(result.body.message).toContainEqual("email must be an email");
  });

  it("should not create a user with an existing email", async () => {
    const user: UserSeeder = userSeedManager.nonAdmins[0];
    const newUser = {
      name: "New user",
      email: user.entity.email,
      password: "123456",
      roles: [],
    };

    const result = await request(app.getHttpServer())
      .post("/users")
      .send(newUser);

    expect(result.status).toBe(500);
  });

  it("should not create a user with an empty email", async () => {
    const newUser = {
      name: "New user",
      password: "123456",
      roles: [],
    };

    const result = await request(app.getHttpServer())
      .post("/users")
      .send(newUser);

    expect(result.status).toBe(400);
    expect(result.body.message).toContainEqual("email must be an email");
  });

  it("should not create a user with an empty password", async () => {
    const newUser = {
      email: "email@email.com",
      name: "New user",
      roles: [],
    };

    const result = await request(app.getHttpServer())
      .post("/users")
      .send(newUser);

    expect(result.status).toBe(400);
    expect(result.body.message).toContainEqual("password should not be empty");
  });

  it("should not create a user with an empty name", async () => {
    const newUser = {
      email: "email@email.com",
      password: "1234567",
      roles: [],
    };

    const result = await request(app.getHttpServer())
      .post("/users")
      .send(newUser);

    expect(result.status).toBe(400);
    expect(result.body.message).toContainEqual("name should not be empty");
  });

  afterEach(async () => {
    await userSeedManager.clean();
  });

  afterAll(async () => {
    await app.close();
  });
});
