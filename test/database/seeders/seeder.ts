/**
 * Class responsible to create and manipulate an Entity that represents a Seed.
 */
export abstract class Seeder<Entity, SeedType> {
  /** Entity that represents a seed. */
  public entity: Entity;

  /**
   * Constructs a Seeder.
   * @param originalSeed Json with data seed.
   */
  constructor(protected originalSeed: SeedType) {
    this.initialize();
  }

  /**
   * Method called inside the constructor to initialize things. It is highly recommend to call it
   * when overloaded.
   */
  protected initialize(): void {
    this.entity = this.createObject();
  }

  /**
   * @returns the Json seed that create the entity.
   */
  public get seed(): SeedType {
    return this.originalSeed;
  }

  /**
   * Creates an entity based on the json seed.
   * @returns The created Entity.
   */
  protected abstract createObject(): Entity;
}
