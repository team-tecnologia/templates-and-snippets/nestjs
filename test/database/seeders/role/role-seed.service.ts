import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { SeedService } from "../seed.service";
import { Role } from "../../../../src/role/entities/role.entity";
import { RoleSeedInterface, roleSeeds } from "../../seeds/role.seed";
import { RoleSeeder } from "./role.seeder";

export class RoleSeedService extends SeedService<Role, RoleSeeder> {
  constructor(@InjectRepository(Role) roleRepository: Repository<Role>) {
    super(roleRepository);
  }

  /**
   * Gets an Role by the given name
   * @param roleName Name of a role to search
   * @returns A Role object
   */
  public getRoleByName(roleName: string): Role {
    const roleSeeder: RoleSeeder = this.all.find((role) => role.entity.name === roleName);
    return roleSeeder.entity;
  }

  public get dataSeed(): RoleSeedInterface[] {
    return roleSeeds;
  }

  protected generateObjects(): void {
    for (const seed of this.dataSeed) {
      const roleSeeder: RoleSeeder = new RoleSeeder(seed);
      this.seeds.push(roleSeeder);
    }
  }
}
