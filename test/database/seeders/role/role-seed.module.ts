import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserModule } from "../../../../src/users/user.module";
import { RoleSeedService } from "./role-seed.service";
import { Role } from "../../../../src/role/entities/role.entity";
import { RoleModule } from "../../../../src/role/role.module";

@Module({
  providers: [RoleSeedService],
  imports: [
    TypeOrmModule.forFeature([Role]),
    UserModule,
    RoleModule,
  ],
  exports: [RoleSeedService],
})
export class RoleSeedModule {}
