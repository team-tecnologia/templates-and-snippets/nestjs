import { Role } from "../../../../src/role/entities/role.entity";
import { RoleSeedInterface } from "../../seeds/role.seed";
import { Seeder } from "../seeder";

export class RoleSeeder extends Seeder<Role, RoleSeedInterface> {
  constructor(roleSeed: RoleSeedInterface) {
    super(roleSeed);
  }

  protected createObject(): Role {
    const role: Role = new Role();
    role.id = this.seed.id;
    role.name = this.seed.name;

    return role;
  }
}
