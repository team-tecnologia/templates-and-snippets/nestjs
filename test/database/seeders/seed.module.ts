import { Module } from "@nestjs/common";
import { RoleSeedModule } from "./role/role-seed.module";
import { UserSeedModule } from "./user/user-seed.module";

@Module({
  imports: [
    RoleSeedModule,
    UserSeedModule,
  ],
  exports: [
    RoleSeedModule,
    UserSeedModule,
  ],
})
export class SeedModule {}
