import { Repository } from "typeorm";
import { SeedInterface } from "../seeds/seed.interface";
import { Seeder } from "./seeder";

/**
 * Class used to manage the seeds and its Entities representations.
 */
export abstract class SeedService<Entity, Seed extends Seeder<Entity, SeedInterface>> {
  /** Array of Seeders. */
  protected seeds: Seed[] = [];

  /**
   * Creates a SeedService
   * @param repository Entity's repository to manipulate database.
   */
  constructor(protected repository: Repository<Entity>) {
    this.initialize();
  }

  /**
   * Saves each object in database.
   * @param checkWasPopulated true to populate only if was not populate before.
   *                          Defaults to false.
   */
  public async populate(checkWasPopulated = false): Promise<void> {
    const wasPopulated: boolean = await this.wasPopulated();
    if (!checkWasPopulated || !wasPopulated) {
      for (const entity of this.entities) {
        await this.repository.save(entity);
      }
    }
  }

  /**
   * Deletes all objects in database.
   */
  public async clean(): Promise<void> {
    await this.repository.clear();
  }

  /**
   * @returns The array containing the seeders.
   */
  public get all(): Seed[] {
    return this.seeds;
  }

  /**
   * @returns An array containing all entities.
   */
  public get entities(): Entity[] {
    const objects: Entity[] = [];
    for (const seed of this.all) {
      objects.push(seed.entity);
    }

    return objects;
  }

  /**
   * Method called inside the constructor to initialize things. It is highly recommend to call it
   * when overloaded.
   */
  protected initialize(): void {
    this.generateObjects();
  }

  /**
   * @returns true if the table count corresponds to seed length
   */
  private async wasPopulated() {
    const count: number = await this.repository.count();
    return this.entities.length === count;
  }

  /**
   * @returns An array containing the original seed json.
   */
  public abstract get dataSeed(): SeedInterface[];

  /**
   * Generate each Seeder
   */
  protected abstract generateObjects(): void;
}
