import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserModule } from "../../../../src/users/user.module";
import { UserSeedService } from "./user-seed.service";
import { User } from "../../../../src/users/entities/user.entity";
import { RoleSeedModule } from "../role/role-seed.module";

@Module({
  providers: [UserSeedService],
  imports: [
    TypeOrmModule.forFeature([User]),
    UserModule,
    RoleSeedModule,
  ],
  exports: [UserSeedService],
})
export class UserSeedModule {}
