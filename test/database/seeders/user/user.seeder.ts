import { User } from "../../../../src/users/entities/user.entity";
import { UserSeedInterface } from "../../seeds/user.seed";
import { Seeder } from "../seeder";
import { RoleEnum } from "../../../../src/auth/types";
import { RoleSeedInterface } from "../../seeds/role.seed";
import { Role } from "../../../../src/role/entities/role.entity";
import { RoleSeedService } from "../role/role-seed.service";

export class UserSeeder extends Seeder<User, UserSeedInterface> {
  public constructor(private roleSeedService: RoleSeedService,
                     userSeed: UserSeedInterface) {
    super(userSeed);
    this.entity = this.createObject();
  }

  /**
   * @returns True if the user has the role Admin.
   */
  public get isAdmin(): boolean {
    return this.entity.roles.some((role) => role.name === RoleEnum.Admin);
  }

  /**
   * @returns The user object expected when a HTTP request is made.
   */
  public get responseHttp(): { [key: string]: any } {
    delete this.seed.password;
    return this.seed;
  }

  public get seed(): UserSeedInterface {
    return this.originalSeed;
  }

  protected createObject(): User {
    const user = new User();
    user.id = this.seed.id;
    user.email = this.seed.email;
    user.name = this.seed.name;
    user.password = this.seed.password;
    user.roles = this.getRolesFromSeeds(this.seed.roles);

    return user;
  }

  /**
   * Creates an array of Role from the json that represents it.
   * @param seeds Seed object that represents a role.
   * @returns An array of Role.
   */
  private getRolesFromSeeds(seeds: RoleSeedInterface[]): Role[] {
    const roles: Role[] = [];
    for (const seed of seeds) {
      const role: Role = this.roleSeedService.getRoleByName(seed.name);
      roles.push(role);
    }

    return roles;
  }

  protected initialize(): void {
    // do nothing
  }
}
