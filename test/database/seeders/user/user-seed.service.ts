import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "../../../../src/users/entities/user.entity";
import { SeedService } from "../seed.service";
import { UserSeedInterface, userSeeds } from "../../seeds/user.seed";
import { UserSeeder } from "./user.seeder";
import { RoleSeedService } from "../role/role-seed.service";

export class UserSeedService extends SeedService<User, UserSeeder> {
  /**
   * Constructs a UserSeedService object.
   * @param roleRepository Role repository to manipulate database.
   * @param userRepository User repository to manipulate database.
   */
  constructor(public roleSeedService: RoleSeedService,
              @InjectRepository(User) userRepository: Repository<User>) {
    super(userRepository);
    this.generateObjects();
  }

  public async clean(): Promise<void> {
    await super.clean();
    await this.roleSeedService.clean();
  }

  public async populate(checkWasPopulated = false): Promise<void> {
    await this.roleSeedService.populate(checkWasPopulated);
    await super.populate(checkWasPopulated);
  }

  public get dataSeed(): UserSeedInterface[] {
    return userSeeds;
  }

  /**
   * @returns All users having role Admin.
   */
  public get admins(): UserSeeder[] {
    return this.all.filter((user) => user.isAdmin);
  }

  /**
   * @returns All users that do not have role Admin.
   */
  public get nonAdmins(): UserSeeder[] {
    return this.all.filter((user) => !user.isAdmin);
  }

  protected generateObjects(): void {
    for (const seed of this.dataSeed) {
      const userSeed: UserSeeder = new UserSeeder(this.roleSeedService, seed);
      this.seeds.push(userSeed);
    }
  }

  protected initialize(): void {
    // do nothing
  }
}
