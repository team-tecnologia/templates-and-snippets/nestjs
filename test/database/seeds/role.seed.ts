import { RoleEnum } from "../../../src/auth/types";
import { SeedInterface } from "./seed.interface";
import { UserSeedInterface, userSeeds } from "./user.seed";

export interface RoleSeedInterface extends SeedInterface {
  name: string,
  users?: UserSeedInterface[],
}

export const roleSeeds: RoleSeedInterface[] = [
  {
    id: "f615b7b2-2cf1-4d16-be0a-9d908b1508b9",
    name: RoleEnum.Admin,
  },
  {
    id: "4356affa-6e57-4c9b-88da-f6c75dca0666",
    name: RoleEnum.User,
  },
];
