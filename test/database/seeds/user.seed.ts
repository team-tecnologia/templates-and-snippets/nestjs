import { SeedInterface } from "./seed.interface";
import { RoleSeedInterface, roleSeeds } from "./role.seed";

export interface UserSeedInterface extends SeedInterface {
  name: string,
  email: string,
  password: string,
  roles: RoleSeedInterface[],
}

export const userSeeds: UserSeedInterface[] = [
  {
    id: "1ebde693-781e-4c64-a53d-11dbdcd8afdb",
    name: "Nicole",
    email: "nicole@email.com",
    password: "123456",
    roles: [roleSeeds[0]],
  },
  {
    id: "6f321df2-a0bd-4963-82ae-aff8111264ae",
    name: "Darwin",
    email: "darwin@email.com",
    password: "654321",
    roles: [roleSeeds[1]],
  },
];
