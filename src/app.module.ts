import { Module, ModuleMetadata } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule, TypeOrmModuleOptions } from "@nestjs/typeorm";
import { ThrottlerGuard, ThrottlerModule } from "@nestjs/throttler";
import { APP_GUARD } from "@nestjs/core";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { AuthModule } from "./auth/auth.module";
import { UserModule } from "./users/user.module";
import { RoleModule } from "./role/role.module";
import { SeedModule } from "../test/database/seeders/seed.module";

export const ModuleConfig = (): ModuleMetadata => {
  const module: ModuleMetadata = {
    imports: [
      SeedModule,
      AuthModule,
      UserModule,
      RoleModule,
      ConfigModule.forRoot({ isGlobal: true }),
      ThrottlerModule.forRoot({ ttl: 60, limit: 10 }),
      TypeOrmModule.forRoot({
        type: process.env.TYPE as "sqlite" | "postgres",
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME,
        autoLoadEntities: true,
        synchronize: true,
      }),
    ],
    controllers: [AppController],
    providers: [AppService, { provide: APP_GUARD, useClass: ThrottlerGuard }],
  };

  return module;
};

@Module(ModuleConfig())
export class AppModule {}
