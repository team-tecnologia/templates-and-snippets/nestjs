import { TestingModule } from "@nestjs/testing";
import { Repository } from "typeorm";
import { AuthService } from "./auth.service";
import { DefaultAppModuleConfig } from "../../test/default-test-config";
import { User } from "../users/entities/user.entity";
import { Role } from "../role/entities/role.entity";
import { UserSeedService } from "../../test/database/seeders/user/user-seed.service";
import { UserSeeder } from "../../test/database/seeders/user/user.seeder";

describe("AuthService", () => {
  let service: AuthService;
  let userSeedManager: UserSeedService;

  beforeAll(async () => {
    const module: TestingModule = await DefaultAppModuleConfig.getModuleFixture();

    service = module.get<AuthService>(AuthService);
    const userRepository: Repository<User> = module.get("UserRepository");
    const roleRepository: Repository<Role> = module.get("RoleRepository");

    userSeedManager = new UserSeedService(roleRepository, userRepository);
  });

  beforeEach(async () => {
    await userSeedManager.populate();
  });

  it("should validate a user", async () => {
    const user: UserSeeder = userSeedManager.all[1];
    const expectedResult = {
      id: user.entity.id,
      name: user.entity.name,
      email: user.entity.email,
      roles: user.entity.roles,
    };

    const result = await service.validateUser(user.seed.email, user.seed.password);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should return null when email does not exist", async () => {
    const user: User = userSeedManager.all[0].entity;

    const result = await service.validateUser(
      "email@invalid.com",
      user.password,
    );

    expect(result).toBeNull();
  });

  it("should return null when password is wrong", async () => {
    const user: User = userSeedManager.all[0].entity;
    const result = await service.validateUser(user.email, "user.password");
    expect(result).toBeNull();
  });

  it("should return null when undefined password is passed", async () => {
    const user: User = userSeedManager.all[0].entity;
    const result = await service.validateUser(user.email, undefined);
    expect(result).toBeNull();
  });

  it("should return null when undefined email is passed", async () => {
    const user: User = userSeedManager.all[0].entity;
    const result = await service.validateUser(undefined, user.password);
    expect(result).toBeNull();
  });

  it("should return null when undefined email and password are passed", async () => {
    const result = await service.validateUser(undefined, undefined);
    expect(result).toBeNull();
  });

  it("should return a token", () => {
    const user: User = userSeedManager.all[0].entity;
    const result = service.login(user);
    expect(result.access_token).toBeDefined();
  });

  afterEach(async () => {
    await userSeedManager.clean();
  });
});
