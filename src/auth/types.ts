import { Role } from "../role/entities/role.entity";

export type JWTPayload = {
  sub: number;
  email: string;
  roles: Role[];
};

export enum RoleEnum {
  User = "user",
  Admin = "admin",
}
