import {
  Controller, HttpCode, Post, Request, UseGuards,
} from "@nestjs/common";
import { ApiBody } from "@nestjs/swagger";
import { LocalAuthGuard } from "./guards/local-auth.guard";
import { AuthService } from "./auth.service";
import { ApiDocs } from "./decorators/api-docs.decorator";
import { LoggedInUser } from "./decorators/logged-user.decorators";
import { User } from "../users/entities/user.entity";

@Controller("auth")
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiDocs({ summary: "Authenticate a user" }, true)
  @ApiBody({
    schema: {
      properties: {
        email: { type: "string", example: "email@email.com", required: ["email"] },
        password: { type: "string", required: ["email"] },
      },
    },
  })
  @UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @Post("/login")
  login(@LoggedInUser() loggedUser: User) {
    return this.authService.login(loggedUser);
  }
}
