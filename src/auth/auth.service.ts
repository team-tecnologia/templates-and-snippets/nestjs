import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { User } from "../users/entities/user.entity";
import { UserService } from "../users/user.service";

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  /**
   * Check if the given email and password are valid
   * @async
   * @param email user's email
   * @param password user's password
   * @returns a map containing the id, name and email of the user, if the credentials
   *          are valid or null, if not
   */
  public async validateUser(
    email: string,
    password: string,
  ): Promise<{ [key: string]: unknown }> {
    const user: User = await this.userService.findByEmail(email);
    if (user && (await user.checkPassword(password))) {
      const result = {
        id: user.id,
        name: user.name,
        email: user.email,
        roles: user.roles,
      };

      return result;
    }

    return null;
  }

  /**
   * Generate a token to the given user
   *
   * @param user user trying to logging
   * @returns a map containing an access_token
   */
  public login(user: User) {
    const payload = { email: user.email, sub: user.id, roles: user.roles };
    const token: string = this.jwtService.sign(payload, {
      secret: process.env.SECRET_KEY,
    });

    return { access_token: token };
  }
}
