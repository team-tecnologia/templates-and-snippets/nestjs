import { ClassSerializerInterceptor, UseInterceptors, applyDecorators } from "@nestjs/common";
import { ApiBearerAuth, ApiOperation } from "@nestjs/swagger";
import { OperationObject } from "@nestjs/swagger/dist/interfaces/open-api-spec.interface";
import { AllowAnon } from "./allow-anon.decorator";
import { RoleEnum } from "../types";
import { Roles } from "./roles.decorator";

export function ApiDocs(operationOptions: Partial<OperationObject>,
                        allowAnon = false,
                        roles: RoleEnum[] = []) {
  if (allowAnon && roles.length) {
    throw (new Error("Can't allow anon and check permissions"));
  }

  const decorators = [ApiOperation(operationOptions), UseInterceptors(ClassSerializerInterceptor)];

  if (allowAnon) {
    decorators.push(AllowAnon());
  } else {
    decorators.push(ApiBearerAuth());
  }

  if (roles.length) {
    decorators.push(Roles(...roles));
  }

  return applyDecorators(...decorators);
}
