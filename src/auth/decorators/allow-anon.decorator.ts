import { SetMetadata } from "@nestjs/common";

export const ALLOW_ANON = "allowAnon";

/**
 * Custom decorator used to indicate that a route allows anon user
 */
export const AllowAnon = () => SetMetadata(ALLOW_ANON, true);
