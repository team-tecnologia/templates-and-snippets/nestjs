import { SetMetadata } from "@nestjs/common";
import { RoleEnum } from "../types";

export const ROLES_KEY = "roles";

/**
 * Custom decorator used to indicate which roles can access a route
 */
export const Roles = (...roles: RoleEnum[]) => SetMetadata(ROLES_KEY, roles);
