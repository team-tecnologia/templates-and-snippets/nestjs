import { Strategy } from "passport-local";
import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthService } from "../auth.service";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({ usernameField: "email" });
  }

  /**
   * Check if the email and passwords of the user exists in database
   * @param email user's email
   * @param password user's password
   * @returns a map containing the id, name and email of the user
   * @throws UnauthorizedException if the credentials are invalid
   * @async
   */
  private async validate(
    email: string,
    password: string,
  ): Promise<{ [key: string]: unknown }> {
    const user = await this.authService.validateUser(email, password);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
