import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { ROLES_KEY } from "../decorators/roles.decorator";
import { RoleEnum } from "../types";

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  /**
   * Checks if a user has the required roles to access the route
   */
  public canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<RoleEnum[]>(
      ROLES_KEY,
      [context.getHandler(), context.getClass()],
    );

    if (!requiredRoles) {
      return true;
    }

    const { user }: { user: { [key: string]: any } } = context.switchToHttp().getRequest();
    const userRoles: Array<string> = [];

    for (const role of user.roles) {
      userRoles.push(role.name);
    }

    return requiredRoles.some((role) => userRoles.includes(role));
  }
}
