import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from "@nestjs/common";
import { UserService } from "./user.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { ApiDocs } from "../auth/decorators/api-docs.decorator";

@Controller("users")
export class UserController {
  constructor(private readonly usersService: UserService) {}

  @ApiDocs({ summary: "Create user" }, true)
  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @ApiDocs({ summary: "Get all users" })
  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @ApiDocs({ summary: "Get user by id" })
  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.usersService.findOne(id);
  }

  @ApiDocs({ summary: "Update user" })
  @Patch(":id")
  update(@Param("id") id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @ApiDocs({ summary: "Delete user by id" })
  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.usersService.remove(id);
  }
}
