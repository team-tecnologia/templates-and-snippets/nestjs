import * as bycript from "bcrypt";
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  BeforeInsert,
  BeforeUpdate,
  Unique,
} from "typeorm";
import { Exclude } from "class-transformer";
import { Role } from "../../role/entities/role.entity";

@Entity()
@Unique(["email"])
export class User {
  @PrimaryGeneratedColumn("uuid")
    id: string;

  @Column()
    name: string;

  @Column()
    email: string;

  @Column()
  @Exclude()
    password: string;

  @ManyToMany(() => Role, (role) => role.users)
  @JoinTable()
    roles: Role[];

  @BeforeUpdate()
  @BeforeInsert()
  async hashPassword() {
    if (this.password) {
      this.password = await bycript.hash(this.password, 10);
    }
  }

  public async checkPassword(password: string): Promise<boolean> {
    if (password) {
      return bycript.compare(password, this.password);
    }
    return false;
  }
}
