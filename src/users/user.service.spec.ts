import { TestingModule } from "@nestjs/testing";
import { Repository } from "typeorm";
import { UserService } from "./user.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { DefaultAppModuleConfig } from "../../test/default-test-config";
import { User } from "./entities/user.entity";
import { Role } from "../role/entities/role.entity";
import { UpdateUserDto } from "./dto/update-user.dto";
import { UserSeedService } from "../../test/database/seeders/user/user-seed.service";

describe("UserService", () => {
  let service: UserService;
  let userSeedManager: UserSeedService;

  beforeAll(async () => {
    const module: TestingModule = await DefaultAppModuleConfig.getModuleFixture();

    service = module.get<UserService>(UserService);
    const userRepository: Repository<User> = module.get("UserRepository");
    const roleRepository: Repository<Role> = module.get("RoleRepository");

    userSeedManager = new UserSeedService(roleRepository, userRepository);
  });

  beforeEach(async () => {
    await userSeedManager.populate();
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  it("should create a user", async () => {
    const userDto: CreateUserDto = new CreateUserDto();
    userDto.email = "user@test.com";
    userDto.password = "12345";
    userDto.name = "User Test";
    userDto.roles = [];

    const result = await service.create(userDto);
    expect(result.id).toBeDefined();
  });

  it("should get all users", async () => {
    const result = await service.findAll();
    expect(result.length).toBe(userSeedManager.all.length);
  });

  it("should get a user by id", async () => {
    const user: User = userSeedManager.all[0].entity;

    const result = await service.findOne(user.id);
    expect(result).toBeDefined();
    expect(result.email).toBe(user.email);
  });

  it("should get a user by email", async () => {
    const user: User = userSeedManager.all[0].entity;

    const result = await service.findByEmail(user.email);
    expect(result).toBeDefined();
    expect(result.id).toBe(user.id);
  });

  it("should update a user", async () => {
    const user: User = userSeedManager.all[0].entity;
    const newUserName = "New User Name";

    const updateUserDto: UpdateUserDto = new UpdateUserDto();
    updateUserDto.name = newUserName;

    const result = await service.update(user.id, updateUserDto);
    expect(result.affected).toBe(1);

    const updatedUser: User = await service.findOne(user.id);
    expect(updatedUser.name).toBe(newUserName);
  });

  it("should delete a user", async () => {
    const user: User = userSeedManager.all[0].entity;
    const result = await service.remove(user.id);
    expect(result.affected).toBe(1);

    const users: Array<User> = await service.findAll();
    expect(users.length).toBe(userSeedManager.all.length - 1);
  });

  afterEach(async () => {
    await userSeedManager.clean();
  });
});
