import { Injectable } from "@nestjs/common";
import { Repository, UpdateResult, DeleteResult } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "./entities/user.entity";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  /**
   * Create a user
   * @param createUserDto user to be created
   * @returns a promise that resolves to the created user
   */
  public create(createUserDto: CreateUserDto): Promise<User> {
    return this.userRepository.save(this.userRepository.create(createUserDto));
  }

  /**
   * Get all users
   * @returns a promise that resolves to an array of all users
   */
  public findAll(): Promise<Array<User>> {
    return this.userRepository.find({
      relations: { roles: true },
    });
  }

  /**
   * Search a user by the given id
   * @param id user id
   * @returns a promise that resolve to the found User or undefined, if not found
   */
  public findOne(id: string): Promise<User | undefined> {
    return this.userRepository.findOne({
      relations: { roles: true },
      where: { id },
    });
  }

  /**
   * Search a user by the given email
   * @param email user email
   * @returns a promise that resolves to the found User or undefined, if not found
   */
  public findByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({
      where: { email },
      relations: { roles: true },
    });
  }

  /**
   * Update a user with the given id
   * @param id user id to be updated
   * @param updateUserDto data that will be changed
   * @returns a promise that resolves to an UpdateResult
   */
  public async update(
    id: string,
    updateUserDto: UpdateUserDto,
  ): Promise<UpdateResult> {
    // do this to trigger @BeforeUpdate on User entity
    const user: User = await this.findOne(id);
    Object.assign(user, updateUserDto);
    await this.userRepository.save(user);

    const updateResult: UpdateResult = new UpdateResult();
    updateResult.affected = 1;
    return updateResult;
  }

  /**
   * Delete the user with the given id
   * @param id user id to be deleted
   * @returns a promise that resolves to an DeleteResult
   */
  public remove(id: string): Promise<DeleteResult> {
    return this.userRepository.delete(id);
  }
}
