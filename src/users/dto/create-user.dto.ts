import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from "class-validator";
import { Role } from "../../role/entities/role.entity";

export class CreateUserDto {
  @ApiProperty({ nullable: false })
  @IsNotEmpty()
    name: string;

  @ApiProperty({ nullable: false, example: "email@email.com" })
  @IsEmail()
    email: string;

  @ApiProperty({ nullable: false })
  @IsNotEmpty()
    password: string;

  @ApiProperty({
    minItems: 0, nullable: false, isArray: true, example: "[{\"id\": 1}]",
  })
    roles: Array<Role>;
}
