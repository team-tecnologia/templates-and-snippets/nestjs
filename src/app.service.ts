import { Injectable } from "@nestjs/common";
import { RoleSeedService } from "../test/database/seeders/role/role-seed.service";
import { UserSeedService } from "../test/database/seeders/user/user-seed.service";

@Injectable()
export class AppService {
  constructor(private roleSeedService: RoleSeedService, private userSeedService: UserSeedService) {}

  getHello(): string {
    return "Hello World!";
  }

  public async populateDatabase() {
    await this.userSeedService.populate(true);
    await this.roleSeedService.populate(true);
  }
}
