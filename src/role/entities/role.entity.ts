import {
  Column, Entity, ManyToMany, PrimaryGeneratedColumn,
} from "typeorm";
import { User } from "../../users/entities/user.entity";

@Entity()
export class Role {
  @PrimaryGeneratedColumn("uuid")
    id: string;

  @Column()
    name: string;

  @ManyToMany(() => User, (user) => user.roles, { cascade: true })
    users: User[];
}
