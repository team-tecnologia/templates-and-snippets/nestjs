import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Repository, UpdateResult } from "typeorm";
import { CreateRoleDto } from "./dto/create-role.dto";
import { UpdateRoleDto } from "./dto/update-role.dto";
import { Role } from "./entities/role.entity";

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role) private roleRepository: Repository<Role>,
  ) {}

  /**
   * Create a role
   * @param createRoleDto role to be created
   * @returns a promise that resolves to the created role
   */
  public create(createRoleDto: CreateRoleDto): Promise<Role> {
    return this.roleRepository.save(createRoleDto);
  }

  /**
   * Get all roles
   * @returns a promise that resolves to an array of all roles
   */
  public findAll(): Promise<Array<Role>> {
    return this.roleRepository.find();
  }

  /**
   * Search a role by the given id
   * @param id role id
   * @returns a promise that resolve to the found Role or undefined, if not found
   */
  public findOne(id: string): Promise<Role | undefined> {
    return this.roleRepository.findOne({
      where: { id },
    });
  }

  /**
   * Update a role with the given id
   * @param id role id to be updated
   * @param updateRoleDto data that will be changed
   * @returns a promise that resolves to an UpdateResult
   */
  public update(
    id: string,
    updateRoleDto: UpdateRoleDto,
  ): Promise<UpdateResult> {
    return this.roleRepository.update(id, updateRoleDto);
  }

  /**
   * Delete the role with the given id
   * @param id role id to be deleted
   * @returns a promise that resolves to an DeleteResult
   */
  public async remove(id: string): Promise<DeleteResult> {
    const role: Role = await this.findOne(id);
    role.users = [];

    await this.roleRepository.save(role);
    return this.roleRepository.delete(id);
  }
}
