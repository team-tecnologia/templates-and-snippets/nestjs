import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from "@nestjs/common";
import { RoleService } from "./role.service";
import { CreateRoleDto } from "./dto/create-role.dto";
import { UpdateRoleDto } from "./dto/update-role.dto";
import { Roles } from "../auth/decorators/roles.decorator";
import { RoleEnum } from "../auth/types";
import { ApiDocs } from "../auth/decorators/api-docs.decorator";

@Roles(RoleEnum.Admin)
@Controller("role")
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @ApiDocs({ summary: "Create a role" })
  @Post()
  create(@Body() createRoleDto: CreateRoleDto) {
    return this.roleService.create(createRoleDto);
  }

  @ApiDocs({ summary: "Get all roles" })
  @Get()
  findAll() {
    return this.roleService.findAll();
  }

  @ApiDocs({ summary: "Get user by id" })
  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.roleService.findOne(id);
  }

  @ApiDocs({ summary: "Update a role" })
  @Patch(":id")
  update(@Param("id") id: string, @Body() updateRoleDto: UpdateRoleDto) {
    return this.roleService.update(id, updateRoleDto);
  }

  @ApiDocs({ summary: "Delete role by id" })
  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.roleService.remove(id);
  }
}
