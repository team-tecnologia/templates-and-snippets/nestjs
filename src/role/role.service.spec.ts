import { TestingModule } from "@nestjs/testing";
import { Repository } from "typeorm";
import { RoleService } from "./role.service";
import { DefaultAppModuleConfig } from "../../test/default-test-config";
import { User } from "../users/entities/user.entity";
import { Role } from "./entities/role.entity";
import { CreateRoleDto } from "./dto/create-role.dto";
import { UpdateRoleDto } from "./dto/update-role.dto";
import { UserSeedService } from "../../test/database/seeders/user/user-seed.service";
import { RoleSeedService } from "../../test/database/seeders/role/role-seed.service";

describe("RoleService", () => {
  let service: RoleService;
  let roleSeedManager: RoleSeedService;

  beforeAll(async () => {
    const module: TestingModule = await DefaultAppModuleConfig.getModuleFixture();

    service = module.get<RoleService>(RoleService);

    const roleRepository: Repository<Role> = module.get("RoleRepository");
    roleSeedManager = new RoleSeedService(roleRepository);
  });

  beforeEach(async () => {
    await roleSeedManager.populate();
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  it("should create a role", async () => {
    const roleDto: CreateRoleDto = new CreateRoleDto();
    roleDto.name = "new role";

    const result = await service.create(roleDto);
    expect(result.id).toBeDefined();
  });

  it("should get all roles", async () => {
    const result = await service.findAll();
    expect(result.length).toBe(roleSeedManager.all.length);
  });

  it("should get a role by id", async () => {
    const role: Role = roleSeedManager.all[0].entity;

    const result = await service.findOne(role.id);
    expect(result).toBeDefined();
    expect(result.name).toBe(role.name);
  });

  it("should update a role", async () => {
    const role: Role = roleSeedManager.all[0].entity;
    const newRoleName = "New Role Name";

    const updateRoleDto: UpdateRoleDto = new UpdateRoleDto();
    updateRoleDto.name = newRoleName;

    const result = await service.update(role.id, updateRoleDto);
    expect(result.affected).toBe(1);

    const updatedRole: Role = await service.findOne(role.id);
    expect(updatedRole.name).toBe(newRoleName);
  });

  it("should delete a role", async () => {
    const role: Role = roleSeedManager.all[0].entity;
    const result = await service.remove(role.id);
    expect(result.affected).toBe(1);

    const roles: Array<Role> = await service.findAll();
    expect(roles.length).toBe(roleSeedManager.all.length - 1);
  });

  afterEach(async () => {
    await roleSeedManager.clean();
  });
});
