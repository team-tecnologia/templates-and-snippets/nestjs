import { Controller, Get } from "@nestjs/common";
import { AppService } from "./app.service";
import { AllowAnon } from "./auth/decorators/allow-anon.decorator";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @AllowAnon()
  getHello(): string {
    return this.appService.getHello();
  }
}
