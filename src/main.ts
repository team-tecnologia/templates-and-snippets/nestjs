import { NestFactory } from "@nestjs/core";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { ValidationPipe } from "@nestjs/common";
import { env } from "process";
import { AppModule } from "./app.module";
import { AppService } from "./app.service";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const config = new DocumentBuilder()
    .setTitle("NestJS Template")
    .setDescription("The API description")
    .setVersion("1.0")
    .addBearerAuth()
    .addTag("template")
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("api", app, document);

  if (env.NODE_ENV === "development") {
    const service: AppService = app.get(AppService);
    await service.populateDatabase();
  }

  await app.listen(3000);
}
bootstrap();
